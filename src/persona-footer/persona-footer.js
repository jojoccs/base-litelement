import { LitElement, html } from 'lit-element';

class PersonaFooter extends LitElement {

    static get properties(){
        return {
        };
    }

    constructor(){
        super();
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
           <footer><p><strong>@Persona App 2021</strong></p></footer>

        `;
    }
}

customElements.define('persona-footer', PersonaFooter)